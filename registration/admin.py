#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.contrib import admin
from . models import User, Recipe, Image, Rating, Comment


# Register your models here.

class UserAdmin(admin.ModelAdmin):

    list_display = ('username', 'email', 'date_joined')


admin.site.register(User, UserAdmin)
admin.site.register(Recipe)
admin.site.register(Image)
admin.site.register(Rating)
admin.site.register(Comment)
