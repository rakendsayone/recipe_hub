#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from . forms import UserForm, LoginForm
from django.views.decorators.cache import cache_control
from django.views.generic import View
from . models import User, Recipe, Image
import random
import os
from django.http import HttpResponse, JsonResponse
from django.core.mail import send_mail
from django.template.loader import render_to_string


def index(request):
    print("in index function")
    template_name = 'registration/index.html'
    slider = list()
    count = list()
    i = 0
    path = '/home/rakendkr/rakendkr.pythonanywhere.com/media/recipe_pics'
    for x in os.listdir(path):
        if os.path.isfile(os.path.join(path, x)):
            slider.append(x)
            count.append(i)
            i = i + 1
    # print(slider)
    return render(request, template_name, {'demo': slider, 'count': count})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def home(request):
    template_name = 'recipe/recipe_list.html'
    username = request.user
    recipes = Recipe.objects.filter(user_id=username.pk)
    return render(request, template_name, {'username': username,
                  'recipes': recipes})


class UserFormView(View):

    form_class = UserForm
    template_name = 'registration/registration_form.html'

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, request):
        form = self.form_class(request.POST, request.FILES)

        if form.is_valid():

            user = form.save(commit=False)

            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password = form.cleaned_data['password']
            password2 = form.cleaned_data['password2']
            profile_pic = request.FILES.get('profile_pic')
            if password == password2:

                user.set_password(password)
                user.save()

            user = authenticate(username=username, password=password)

            if user is not None:

                if user.is_active:
                    login(request, user)

                    # return render(request, 'registration/user_home.html'
                    #               , {'username': username})

                    return redirect('recipe:recipe-list')

        return render(request, self.template_name, {'form': form})


class LoginFormView(View):

    form_class = LoginForm
    template_name = 'registration/login_form.html'

    # @login_required(login_url='registration/login_form.html')

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    # @login_required(login_url='registration/login_form.html')

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, request):

        error_message = ''
        form = self.form_class(request.POST)

        if form.is_valid():

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:

                    # template_name = 'recipe/recipe_list.html'

                    login(request, user)
                    user_instance = User.objects.get(pk=request.user.pk)

                    return redirect('recipe:recipe-list')
            else:
                error_message = \
                    'Invalid User credentials, please check and login again'
                return render(request, self.template_name,
                              {'error_message': error_message,
                               'form': form})
        return render(request, self.template_name, {'form': form})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def logout_view(request):
    logout(request)
    return redirect('registration:user-login')


def about_us(request):
    return render(request, 'registration/about_us.html')


def contact_us(request):
    if request.method == 'POST':
        subject = request.POST['subject']
        from_address = request.POST['email']
        name = request.POST['name']
        message = request.POST['message']
        send_mail(
                subject + " - " + name,
                message,
                from_address,
                ['rakend.sayone@gmail.com'],
                fail_silently=False,
            )
        return JsonResponse({'message': 'Your mail has been send successfully.'})
    return render(request, 'registration/contact_form.html')


def search(request):
    word = request.GET['word']
    recipe = Recipe.objects.filter(name__icontains=word)
    for item in recipe:
        image = Image.objects.filter(recipe=item.pk)
    context = {'recipe': recipe, 'image': image}
    html = render_to_string('registration/search-result.html', context)
    return JsonResponse({'html': html})
