#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.utils import timezone
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator


class User(AbstractUser):

    profile_pic = models.FileField(upload_to='profile_pics/',
                                   blank=True,
                                   default='profile_pics/user.png')


class Recipe(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=False)
    incredient = models.CharField(max_length=1500, blank=False)
    recipe = models.CharField(max_length=1500, blank=False)
    video = models.FileField(upload_to='videos/', max_length=500,
                             blank=True)
    created = models.DateTimeField('Date Posted', default=timezone.now)

    def __str__(self):
        return self.name


class Image(models.Model):

    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    image = models.FileField(upload_to='recipe_pics/', max_length=300,
                             blank=True)

    def __str__(self):
        return str(self.recipe)


class Rating(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    value = models.IntegerField(
        default=0, validators=[MaxValueValidator(5), MinValueValidator(0)])

    def __str__(self):
        if(self.value == 1):
            rating_text = str(self.user) + " rated " + str(self.recipe) + " for " + str(self.value) + " star."
        else:
            rating_text = str(self.user) + " rated " + str(self.recipe) + " for " + str(self.value) + " stars."

        return rating_text


class Comment(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    comment = models.CharField(max_length=200)
    created = models.DateTimeField('Date Posted', default=timezone.now)

    def __str__(self):
        return str(self.user) + " commented on recipe " + str(self.recipe)
