#!/usr/bin/python
# -*- coding: utf-8 -*-
from . models import User
from django import forms


class UserForm(forms.ModelForm):

    username = forms.CharField(max_length=20, label='Username')
    password = forms.CharField(max_length=20,
                               widget=forms.PasswordInput(),
                               label='Password')
    password2 = forms.CharField(max_length=20,
                                widget=forms.PasswordInput(),
                                label='Confirm Password')

    class Meta:

        model = User
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
            'password',
            'password2',
            'profile_pic',
            ]


class LoginForm(forms.Form):

    username = forms.CharField(max_length=20)
    password = forms.CharField(max_length=20,
                               widget=forms.PasswordInput())

    class Meta:

        model = User

        fields = ['username', 'password']
