#!/usr/bin/python
# -*- coding: utf-8 -*-
from . import views
from django.urls import path

app_name = 'registration'

urlpatterns = [path('', views.index, name='index'), path('register',
               views.UserFormView.as_view(), name='register'),
               path('login', views.LoginFormView.as_view(),
               name='user-login'), path('logout', views.logout_view,
               name='logout'), path('home', views.home, name='home'),
               path('about-us', views.about_us, name='about-us'),
               path('contact-us', views.contact_us, name='contact-us'),
               path('search', views.search, name='search')]
