from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
# from django.views.generic.base import TemplateView
from registration import views

urlpatterns = [path('admin/', admin.site.urls), path('',
               views.index, name='index'), path('registration/',
               include('registration.urls')), path('recipe/',
               include('recipe.urls'))] + static(
                   settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
