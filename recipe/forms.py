#!/usr/bin/python
# -*- coding: utf-8 -*-
from registration.models import User, Recipe, Image, Rating
from django import forms


class RecipeForm(forms.ModelForm):

    name = forms.CharField(max_length=50,
                           widget=forms.TextInput(attrs={
                               'class': 'form-control'}), label='Recipe Name')
    incredient = forms.CharField(max_length=1500,
                                 widget=forms.Textarea(attrs={
                               'class': 'form-control'}),
                                 label='Incredients')
    recipe = forms.CharField(max_length=1500, widget=forms.Textarea(attrs={
                               'class': 'form-control'}),
                             label='Preparation')
    video = forms.FileField(max_length=500, widget=forms.FileInput(attrs={
                               'class': 'form-control'}),
                            label='Video')
    image = forms.FileField(max_length=500,
                            widget=forms.FileInput(attrs={'multiple': '',
                                'class': 'form-control'}),
                            label='Image')

    class Meta:

        model = Recipe
        fields = ['name', 'incredient', 'recipe', 'video']


class EditUserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'profile_pic']
