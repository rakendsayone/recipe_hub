#!/usr/bin/python
# -*- coding: utf-8 -*-
from . import views
from django.urls import path

app_name = 'recipe'

urlpatterns = [path('add-recipe', views.RecipeFormView.as_view(),
               name='add-recipe'), path('recipe-list',
               views.recipe_list, name='recipe-list'),
               path('recipe-details/<int:pk>', views.recipe_details,
               name='recipe-details'), path('recipe-detail-list',
               views.recipe_detail_list, name='recipe-detail-list'),
               path('recipe-download/<int:pk>', views.generate_pdf,
               name='recipe-download'), path('404-error', views.error_page,
               name='404-error'), path('rating', views.rating, name='rating'),
               path('comments', views.add_comment, name='comments'),
               path('user-profile', views.view_profile, name='user-profile'),
               path('edit-profile', views.edit_profile, name='edit-profile'),
               path('media-files', views.show_media_files, name='media-files'),
               # path('demo', views.demo, name='demo')
               ]
