#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import template
from django.db.models import Avg
from registration.models import Recipe, Image, Rating, Comment
import math

register = template.Library()


@register.simple_tag(takes_context=True)
def recipe_images(context, recipe_id):
    images = Image.objects.filter(recipe_id=recipe_id)
    pics = list()
    for img in images:
        pics.append(img.image.url)
    return pics


@register.simple_tag(takes_context=True)
def rating_star(context, recipe_id):
    current_rating = Rating.objects.filter(
        recipe_id=recipe_id).aggregate(Avg('value'))
    rating = current_rating['value__avg']
    return int(rating)


@register.simple_tag(takes_context=True)
def recipe_comment(context, recipe_id):
    comments = Comment.objects.filter(recipe_id=recipe_id).order_by('-created')
    return comments
