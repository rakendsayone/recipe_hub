#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import RecipeForm, EditUserForm
from django.views.decorators.cache import cache_control
from django.contrib.auth.decorators import login_required
from el_pagination.decorators import page_template
from django.views.generic import View
from registration.models import Recipe, Image, Rating, Comment, User
from django.db.models import Avg
from io import BytesIO
from django.http import HttpResponse, JsonResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
import random
import os
from django.template.defaultfilters import slugify


class RecipeFormView(View):

    form_class = RecipeForm
    template_name = 'recipe/recipe_form.html'

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def get(self, request):
        form = self.form_class(None)
        username = request.user
        return render(request, self.template_name, {'form': form,
                      'username': username})

    @cache_control(no_cache=True, must_revalidate=True, no_store=True)
    def post(self, request):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():

            new_recipe = form.save(commit=False)
            new_recipe.user = request.user
            name = form.cleaned_data['name']
            incredient = form.cleaned_data['incredient']
            recipe = form.cleaned_data['recipe']
            video = request.FILES.get('video')
            new_recipe.save()
            for pics in request.FILES.getlist('image'):
                recipe_image = Image(recipe=new_recipe, image=pics)
                recipe_image.save()
        return render(request, self.template_name, {'form': form})


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def recipe_list(request):
    template_name = 'recipe/recipe_list.html'
    username = request.user
    recipe = Recipe.objects.all()
    word = request.GET.get('word', False)
    if word:
        recipe = Recipe.objects.filter(name__icontains=word)
    context = {'username': username,
                  'recipe': recipe}
    return render(request, template_name, context)


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def recipe_details(request, pk):
    template_name = 'recipe/recipe_details.html'
    username = request.user
    recipe = Recipe.objects.filter(pk=pk)
    rating = Rating.objects.filter(recipe=pk).aggregate(Avg('value'))
    if recipe:
        image = Image.objects.filter(recipe=pk)
        rating_count = Rating.objects.filter(recipe=pk).count()
        if rating_count == 0:
            rating_text = 'Rate the recipe 1st.'
        else:
            rating_text = 'Average Rating :' + str(rating['value__avg'])
        context = {'username': username, 'recipe': recipe,
                   'image': image, 'avg_rating': rating_text}
    else:
        return redirect('recipe:404-error')
    return render(request, template_name, context)


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def recipe_detail_list(request, context):
    return render(request, 'recipe/recipe_details.html', context)


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(),
                            content_type='application/pdf')
    return None


@login_required
def generate_pdf(request, pk):
    template = 'recipe/recipe_pdf.html'
    current_recipe = Recipe.objects.filter(pk=pk)
    if current_recipe:
        recipe_pic = Image.objects.filter(recipe=pk)
        no = random.randint(10000, 99999)
        for name in current_recipe:
            slug = 'Recipe-Hub-' + name.name + '-' + str(no)
        context = {'recipe': current_recipe, 'pics': recipe_pic}

        pdf = render_to_pdf(template, context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = slugify(slug)
            content = "inline; filename='%s'" % filename
            response['Content-Disposition'] = content
        # return render(request, template, context)
    else:
        return redirect('recipe:404-error')
    return response


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def rating(request):
    user = request.user
    recipe = request.POST.get('recipe')
    recipe_obj = Recipe.objects.get(pk=recipe)
    rating = request.POST.get('rating')
    all_recipes = Rating.objects.filter(user=user, recipe=recipe_obj)
    rating_list = all_recipes.count()
    if rating_list == 0:
        new_rating = Rating(user=user, recipe=recipe_obj, value=rating)
        new_rating.save()
    else:
        new_rating = Rating.objects.filter(user=user, recipe=recipe_obj)
        new_rating.update(value=rating)
    current_recipe = Rating.objects.filter(recipe=recipe_obj)
    avg_rating = current_recipe.aggregate(Avg('value'))
    return JsonResponse({'message': 'Your rating successfully added.',
                         'avg_rating': int(avg_rating['value__avg'])})


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def add_comment(request):
    user = request.user.pk
    recipe = request.POST.get('recipe')
    comment = request.POST.get('comment')
    if comment:
        current_recipe = Recipe.objects.get(pk=recipe)
        current_user = User.objects.get(pk=user)
        new_comment = Comment.objects.create(
            recipe=current_recipe, user=current_user, comment=comment)
        new_comment.save()
        comment = '<p><b>' + str(new_comment.user) + '</b> : ' + new_comment.comment + '</p>'
        return JsonResponse({'comment': comment})
    else:
        msg = "Please enter a comment"
        return JsonResponse({'status': msg})


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def view_profile(request):
    user = request.user.pk
    current_user = User.objects.get(pk=user)
    user_recipe = Recipe.objects.filter(user=user)
    return render(request, 'recipe/profile.html',
                          {'username': request.user,
                           'current_user': current_user,
                           'user_recipe': user_recipe
                           })


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def edit_profile(request):
    form_class = EditUserForm
    if request.method == 'POST':
        form = form_class(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            new_user = form.save()
            return redirect('recipe:user-profile')
    else:
        form = form_class(instance=request.user)
    return render(request, 'recipe/edit_profile.html',
                  {'form': form, 'username': request.user})


def show_media_files(request):
    videos = Recipe.objects.filter(user=request.user)
    for video in videos:
        images = Image.objects.filter(recipe=video)
    return render(request, 'recipe/media.html',
                  {'images': images, 'videos': videos})


def error_page(request):
    return render(request, 'recipe/error404.html')


# def demo(request):
#     slider = list()
#     count = list()
#     i = 0
#     path = '/home/djr5/recipe_hub/media/recipe_pics'
#     for x in os.listdir(path):
#         if os.path.isfile(os.path.join(path, x)):
#             slider.append(x)
#             count.append(i)
#             i = i + 1
#     return render(request, 'recipe/comments.html', {'demo': slider, 'count': count})
